"use strict";
const http = require("http");

const handlerFunc = (request, response) => {
    if (request.url==='/echo'  && request.method === 'POST') {
        request.pipe(response)
    } else {
        response.statusCode=404;
        response.end()
    }
};

http
    .createServer(handlerFunc)
    .listen(3000);


/**
 * Задание 8 - Напишите эхо-сервер, который на запрос POST /echo будет возвращать тело запроса.
 * Напишите функцию которая посылает запрос на http://localhost:3000 POST /echo со следующей
 * полезной нагрузкой:
 * { message: "Привет сервис, я жду от тебя ответа"}
 * Примите ответ от сервера и выведите результат в консоль, используя синтаксис async/await.
 */

