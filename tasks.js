"use strict";
/**
 * Задание 1 - Имитируем работу с сервером - Promise
 *
 * 1. Написать функцию getList, которая возвращает Promise с данными о списке задач, иммитируя
 * задержку перед получением в 2 секунды:
 * [
 * { id: 1, title: 'Task 1', isDone: false },
 * { id: 2, title: 'Task 2', isDone: true },
 * ]
 * 2. Написать скрипт который получит данные из функции getList и выведет на экран список задач.
 * 3. Изменить промис так, чтобы он возвращал ошибку
 * 4. Дополнить скрипт так, чтобы если промис возвращает ошибку выводилось сообщение об ошибке
 */
// function getlist(){
//     return new Promise(function(resolve, reject) {
//         const result = [
//             {id: 1, title: 'Task 1', isDone: false},
//             {id: 2, title: 'Task 2', isDone: true},
//         ];
//         // setTimeout(() => resolve(result), 2000);
//         reject("Sorry, it's a mistake");
//     });
// }
// const taskList=getlist();
// taskList.then(
//     function(result){
//        console.log(result);
//     },
//     function(error){
//         console.error(error);
//     }
// );

/**
 * Задание 2 - Чейнинг (цепочки) промисов
 *
 * Написать функцию которая будет соберет строку "Я использую цепочки обещаний", конкотенируя каждое
 * слово через отдельный then блок с задержкой в 1 секунду на каждой итерации.
 * Результат вывести в консоль.
 */

// new Promise(function(resolve, reject) {
//     setTimeout(() => resolve('Я'), 1000);
// }).then(function(result) {
//     console.log(result);
//     return new Promise((resolve, reject) => {
//         setTimeout(() => resolve(result + ' использую'), 1000);
//     });
// }).then(function(result) {
//    console.log(result);
//     return new Promise((resolve, reject) => {
//         setTimeout(() => resolve(result + ' цепочки' ), 1000);
//     });
// }).then(function(result) {
//     console.log(result);
//     return new Promise((resolve, reject) => {
//         setTimeout(() => resolve(result + ' обещаний' ), 1000);
//     });
// }).then(function(result) {
//     console.log(result);
// });

/**
 * Задание 3 - Параллельные обещания
 *
 * Написать функцию которая будет соберет строку "Я использую вызов обещаний параллельно",
 * используя функцию Promise.all(). Укажите следующее время задержки для каждого
 * промиса возвращаего слова:
 * Я - 1000,
 * использую - 800
 * вызов - 1200
 * обещаний - 700
 * параллельно - 500
 * Результат вывести в консоль.
 */

// Promise.all([
//     new Promise(resolve => setTimeout(() => resolve('Я'), 1000)),
//     new Promise(resolve => setTimeout(() => resolve('использую'), 800)),
//     new Promise(resolve => setTimeout(() => resolve('вызов'), 1200)),
//     new Promise(resolve => setTimeout(() => resolve('обещаний'), 700)),
//     new Promise(resolve => setTimeout(() => resolve('параллельно'), 500))
// ]).then(console.log);

/**
 * Задание 4 - Напишите функцию delay(ms), которая возвращает промис,
 * переходящий в состояние "resolved" через ms миллисекунд.
 *
 * delay(2000).then(() => console.log('Это сообщение вывелось через 2 секунды'))
 */

function delay(ms) {
    return new Promise((resolve, reject) => {
        setTimeout(resolve, ms);
    })
}

// delay(2000).then((result) => console.log('Это сообщение вывелось через 2 секунды'));


/**
 * Задание 5 - Решите 3 задачу, используя, функцию delay
 */

// new Promise(resolve => setTimeout(() => resolve('Я'), 1000));
// delay(1000).then(()=>{return '!Я'})

Promise.all([
    delay(1000).then(() => 'Я'),
    delay(800).then(() => 'использую'),
    delay(1200).then(() => 'вызов'),
    delay(700).then(() => 'обещаний'),
    delay(500).then(() => 'параллельно'),
]).then(console.log);


/**
 * Задание 6 - Напишите функцию, которая загрузит данные по первому фильму в котором встретилась планета Татуин, используя
 * предоставленный API (https://swapi.dev)
 */

// fetch('https://swapi.dev/api/planets/1/')
//     .then(response => response.json())
//     .then(console.log);

// async function getFilmName(planetName) {
//     const data = await fetch('https://swapi.dev/api/films');
//     const dataObj = await data.json();
//
//     const films = dataObj.results;
//
//     for(let i = 0; i < films.length; i++) {
//         const planetsData = await Promise.all(
//             films[i].planets.map(planetUrl => fetch(planetUrl).then(res => res.json()))
//         );
//
//
//         if (planetsData.find(planet => planet.name === planetName)) {
//             return films[i].title;
//         }
//     }
//
//     return 'Фильм не найден';
// }
//
// getFilmName('Tatooine').then(filmName => console.log(filmName));

/**
 * Задание 7 - Напишите функцию, которая выведет название транспортного средства на котором впервые ехал Anakin Skywalker,
 * используя
 * предоставленный API (https://swapi.dev)
 */


// fetch('https://swapi.dev/api/people/11/')
//     .then(response => response.json())
//     .then((obj) => { //Anakin
//         return obj.vehicles[0];
//     })
//     .then((vehicleUrl) => {
//         return fetch(vehicleUrl);
//     })
//     .then(response => response.json())
//     .then(vehicleInfo => {
//         console.log(vehicleInfo.name)
//     });

async function _getHeroByUrl(name, url) {
    const response = await fetch(url);
    const heroes = await response.json();
    // console.log(heroes);
    for (let i = 0; i < heroes.results.length; i++) {
        const hero = heroes.results[i];
        if (hero.name === name) {
            return hero;
        }
    }
    if (heroes.next != null) {
        return _getHeroByUrl(name, heroes.next);
    }
    return null;
}

async function getHeroVehicle(name) {
    const heroData = await _getHeroByUrl(name, 'https://swapi.dev/api/people/')
    if (!heroData) {
        return null;
    }
    // console.log(heroData);
    if (heroData.vehicles.length < 0) {
        return null;
    }
    const response = await fetch(heroData.vehicles[0]);
    const vehicleInfo = await response.json();
    return vehicleInfo.name;
}

getHeroVehicle('Anakin Skywalker')
    .then(console.log)