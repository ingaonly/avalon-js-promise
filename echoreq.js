"use strict";

/**
 * Задание 8 - Напишите эхо-сервер, который на запрос POST /echo будет возвращать тело запроса.
 * Напишите функцию которая посылает запрос на http://localhost:3000 POST /echo со следующей
 * полезной нагрузкой:
 * { message: "Привет сервис, я жду от тебя ответа"}
 * Примите ответ от сервера и выведите результат в консоль, используя синтаксис async/await.
 */

const fetch = require("node-fetch");

async function f() {
    const response = await fetch('http://127.0.0.1:3000/echo', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify({
            message: "Привет сервис, я жду от тебя ответа"
        })
    });
    const responseObj = await response.json();
    console.log(responseObj);
}

f();